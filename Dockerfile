FROM golang:latest
RUN mkdir /app
WORKDIR /app
COPY . /app/assigment_1/
RUN cd /app/assigment_1 && go mod download all
RUN cd /app/assigment_1/server && go build 
EXPOSE 8080
CMD ["/app/assigment_1/server/server"]