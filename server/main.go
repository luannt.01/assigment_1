package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"strings"

	// "math/rand"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/luannt.01/assigment_1/ent"
	pb "gitlab.com/luannt.01/assigment_1/pkg/api/v1"
	"google.golang.org/grpc"
)

var (
	port = flag.Int("port", 8080, "The server port")
)

type server struct {
	pb.UnimplementedUserServiceServer
	db *ent.Client
}

func (s *server) Create(ctx context.Context, in *pb.User) (*pb.Status, error) {

	log.Printf("Received: %v", in)

	user, err := s.db.User.Create().
		SetName(in.GetName()).
		SetAge(int32(in.GetAge())).
		SetEmail(in.GetEmail()).
		SetPassword(in.GetPassword()).
		Save(ctx)
	log.Printf("Received: %v", user)
	if err != nil {
		return nil, fmt.Errorf("failed creating user: %w", err)
	}
	res := pb.Status{}
	res.Status = "Success"
	return &res, nil

}

func (s *server) Delete(ctx context.Context, in *pb.Id) (*pb.Status, error) {

	err := s.db.User.DeleteOneID(in.IdUser).Exec(ctx)
	log.Printf("Received: %v", in.IdUser)

	res := pb.Status{}
	if err != nil {
		return nil, fmt.Errorf("failed deleting user: %v", err)
	}
	res.Status = "Success"
	return &res, nil
}

func (s *server) Get(ctx context.Context, in *pb.Id) (*pb.User, error) {
	user, err := s.db.User.Get(ctx, in.GetIdUser())
	if err != nil {
		return nil, fmt.Errorf("failed to get user %d", in.GetIdUser())
	}
	log.Printf("Received: %v", in)

	return &pb.User{
		Id:       user.ID,
		Name:     user.Name,
		Age:      user.Age,
		Email:    user.Email,
		Password: user.Password,
	}, nil
}

func (s *server) Update(ctx context.Context, in *pb.UpdateRequest) (*pb.Status, error) {
	// user, err := s.db.User.UpdateOneID(in.Id).Mutation().SetField(,).Save(ctx)
	res := pb.Status{}

	for _, change := range in.Changes {
		res.Status = "Fail"

		switch a := strings.Split(change.String(), ":"); a[0] {
		case "age":
			_, err := s.db.User.UpdateOneID(in.Id).SetAge(change.GetAge()).Save(ctx)
			if err != nil {
				return nil, fmt.Errorf("update user failed %d %v", in.Id, err)
			}
			res.Status = "Success"
		case "name":
			_, err := s.db.User.UpdateOneID(in.Id).SetName(change.GetName()).Save(ctx)
			if err != nil {
				return nil, fmt.Errorf("update user failed %d %v", in.Id, err)
			}
			res.Status = "Success"
		case "email":
			_, err := s.db.User.UpdateOneID(in.Id).SetEmail(change.GetEmail()).Save(ctx)
			if err != nil {
				return nil, fmt.Errorf("update user failed %d %v", in.Id, err)
			}
			res.Status = "Success"
		case "password":
			_, err := s.db.User.UpdateOneID(in.Id).SetPassword(change.GetPassword()).Save(ctx)
			if err != nil {
				return nil, fmt.Errorf("update user failed %d %v", in.Id, err)
			}
			res.Status = "Success"

		}
	}

	return &res, nil
}

func (s *server) GetAll(ctx context.Context, in *pb.GetAllRequest) (*pb.GetAllResponses, error) {
	re, err := s.db.User.Query().All(ctx)
	if err != nil {
		return nil, err
	}
	var ans []*pb.User

	var count int32 = 0
	for index, user := range re {
		tmp := &pb.User{
			Id:       user.ID,
			Name:     user.Name,
			Age:      user.Age,
			Email:    user.Email,
			Password: user.Password,
		}
		if in.Limit == 0 && int32(index) >= in.Offset {
			ans = append(ans, tmp)
		} else if count < in.Limit && int32(index) >= in.Offset {
			count += 1
			ans = append(ans, tmp)
		}
	}

	return &pb.GetAllResponses{
		User: ans,
	}, nil

}

func main() {
	// initUser()
	client, err := ent.Open("mysql", "root:password@tcp(172.17.0.2:3306)/example?parseTime=True")
	if err != nil {
		log.Fatalf("failed opening connection to sqlite: %v", err)
	}
	log.Printf("Connecion")
	defer client.Close()

	ctx := context.Background()
	// Run the auto migration tool.
	if err := client.Schema.Create(ctx); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}
	// if _, err = CreateUser(ctx, client); err != nil {
	// 	log.Fatal(err)
	// }

	flag.Parse()
	lis, err := net.Listen("tcp", "0.0.0.0:8080")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterUserServiceServer(s, &server{pb.UnimplementedUserServiceServer{}, client})
	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
